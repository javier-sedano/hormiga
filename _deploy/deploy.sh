#!/bin/bash
pushd `dirname "$0"`
for FILE in /var/www/html/*
do
  if [ "$FILE" = "/var/www/html/tortugaWinUpdate" -o "$FILE" = "/var/www/html/ballenaWinUpdate" ]
  then
    echo "Skipping $FILE"
    continue
  fi
  rm -rf "$FILE"
done
mv _site/* /var/www/html/
popd
