---
title: About
layout: default
permalink: /about/
---
(C) Javier Sedano 2019-2024

Contact: [javier.sedano@gmail.com](mailto:javier.sedano@gmail.com)

License: [Creative Commons Attributtion ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

Powered by [Jekyll](https://jekyllrb.com/).

Source code hosted at [GitLab](https://gitlab.com/javier.sedano/hormiga).

