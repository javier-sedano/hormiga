This is the source code for https://jsedano.duckdns.org/. Visit the site for a [description](https://jsedano.duckdns.org/description/) of this solution.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/hormiga/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/hormiga/commits/master)

Keywords: jekyll, CMS, SSG, w3css, responsive, mobile-first, material-icons

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* _tools/initDependencies.sh
* _tools/serve.sh
* Browse http://localhost:4000

Useful links:
* https://jekyllrb.com/
* https://rubygems.org/
