---
title: Description
layout: default
permalink: /description/
---

This page is created using the following technologies:

* The source code is hosted at [GitLab](https://gitlab.com/javier.sedano/hormiga). GitLab is a cloud [Git](https://git-scm.com/) provider with excelent support for Open Source projects and excelent service for commercial projects.

![GitLab](/assets/resources/gitlab.png)

* Continuous Integration is done using [GitLab-CI](https://docs.gitlab.com/ee/ci/). With CI, every push to a branch causes the build of the project. `master` branch is compiled and deployed to the [Production Environment](https://jsedano.duckdns.org/) (see below); every other branch is compiled but not deployed.

![GitLab-CI](/assets/resources/gitlab-ci.png)

* Continuous Deployment is done to the [Production Environment](https://jsedano.duckdns.org/) hosted at [Google Compute Engine](https://cloud.google.com/compute/). Google Compute Engine provides cloud virtual machines with reasonable prices. Acually, a very tiny machine can be used for free... which is the machine that is used for this deployment. So if this page is slow, be patient.

* The page is generated using [Jekyll](https://jekyllrb.com/). Jekyll is a Static Content Management System, or Static Site Generator. It creates the final page allowing reuse of components and easy content generation. As oposed to classic CMSs (such as Wordpress or Joomla), the page is generated at build-time, practically removing the security problems and providing light-fast performance. There are other Static CMSs, but Jekyll is probably the best known solution on this field.

![Jekyll](/assets/resources/jekyll.png)

* The style is based in [W3.CSS](https://www.w3schools.com/w3css/). W3.CSS is a CSS framework, provided by [w3schools](https://www.w3schools.com/), aiming for a soft learning curve, mobile-first responsive design and no javascript requirements. [SCSS](https://sass-lang.com/documentation/syntax) is being supported, but actually currently it is not of much use.

* The development is made with a [Containerized Development Environment](https://code.visualstudio.com/docs/remote/containers) using [Visual Studio Code](https://code.visualstudio.com/). If [Docker](https://www.docker.com/) is installed, VSCode will open the development environment inside a container which is defined within the project itself. This way, all the needs for the development (shell scripting, [Ruby](https://www.ruby-lang.org/en/), [Gems](https://rubygems.org/),...) are fulfilled by this Docker image and the developer does not need to install anything (aside from Docker and VSCode). It even works on Windows if [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows) is installed.

* Content is written in [HTML](https://www.w3schools.com/html/) or [Markdown](https://daringfireball.net/projects/markdown/basics). For example, this description page is written in Markdown, while the home is written in HTML. Markdown is a markup languaje that translates to HTML but uses a simpler syntax, intended for non-programmers who want to create formatted text without the complexity of HTML. *For example*, this is the `Markdown code` for this **very paragraph**:

```
        * Content is written in [HTML](https://www.w3schools.com/html/)
        or [Markdown](https://daringfireball.net/projects/markdown/basics).
        For example, this description page is written in Markdown, while
        the home is written in HTML. Markdown is a markup languaje that
        translates to HTML but uses a simpler syntax, intended for
        non-programmers who want to create formatted text without the
        complexity of HTML. *For example*, this is the `Markdown code` for
        this **very paragraph**:
```

* The icons are provided by [Font Awesome](https://fontawesome.com/). With Font Awesome you can add iconography to your page using CSS classes.
