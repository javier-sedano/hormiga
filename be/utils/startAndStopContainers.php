<?php
require_once '../vendor/autoload.php';

function unathorized() {
    http_response_code(403);
    exit;
}

function checkUser() {
    $CLIENT_ID = '599178223994-vtjh5uq092gq3jk2j452td3v5u7n7ius.apps.googleusercontent.com';
    $JAVIER_SEDANO = '108238071696792553186';

    $authorization = getallheaders()['Authorization'];
    if (!$authorization) {
        unathorized();
    }
    if (substr( $authorization, 0, 7 ) !== "Bearer ") {
        unathorized();
    }
    $idToken = substr($authorization, 7);
    $client = new Google_Client(['client_id' => $CLIENT_ID]);
    $payload = $client->verifyIdToken($idToken);
    if ($payload) {
        $userId = $payload['sub'];
        if ($userId !== $JAVIER_SEDANO) {
            http_response_code(403);
            exit;
        }
    } else {
        http_response_code(403);
        exit;
    }
}

function startContainers() {
    startStopContainers('start');
}

function stopContainers() {
    startStopContainers('stop');
}

function startStopContainers($action) {
    checkUser();
    $containers = $_GET['containers'];
    if ( ($containers === null) || ($containers === '') ) {
        http_response_code(400);
        exit;
    }
    $dockerCommand = getenv('DOCKER_COMMAND');
    $docker = $dockerCommand ? $dockerCommand : 'docker';
    foreach (explode(',', $containers) as $container) {
        $command = "$docker $action $container";
        $result = `$command`;
        echo $result;
        if (!$result) {
            http_response_code(500);
            exit;
        }
    }
}

?>