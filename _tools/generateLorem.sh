#!/bin/bash
pushd `dirname $0`/../_posts

for HH in $(seq -f "%02g" 1 1)
do
    FILE=2019-11-29-loren-$HH.markdown
    #echo $FILE
    TITLE=`curl -s "http://asdfast.beobit.net/api/?type=word&length=3" | jq -r ".text"`
    LOREM=`curl -s "http://asdfast.beobit.net/api/?type=paragraph&length=5&startLorem=true" | jq -r ".text"`
    cat << EOF | sed "s/HH/$HH/g" | sed "s/TITLE/$TITLE/g" > $FILE
---
layout: post
title:  "TITLE"
date:   2019-11-29 09:HH:00 +0000
---
EOF
    echo "$LOREM" >> $FILE
done
popd
