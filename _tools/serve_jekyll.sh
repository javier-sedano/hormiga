#!/bin/bash
pushd `dirname $0`/..
bundle exec jekyll serve -H 127.0.0.2 -P 4100 -V
popd
