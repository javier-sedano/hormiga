#!/bin/bash
pushd `dirname $0`/..

rm -rf `pwd`/be/apache2/
mkdir `pwd`/be/apache2/
mkdir `pwd`/be/apache2/logs/
PROJECT_ROOT=`pwd` APACHE_LOG_DIR=logs DOCKER_COMMAND=`pwd`/_tools/toggle.sh apache2 -f `pwd`/_tools/apache2.conf -D FOREGROUND -E /dev/stderr

popd
