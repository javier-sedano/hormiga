#!/bin/bash -x
pushd `dirname $0`/..
command=$1
dir=$2

if [ -z "$dir" ]
then
  echo "missing dir"
  exit -1
fi

case $command in
"start")
  echo "starting $dir"
  sudo mkdir "_site/$dir"
;;
"stop")
  echo "stoping $dir"
  sudo rm -rf "_site/$dir"
;;
*)
  echo "wrong command"
  exit -1
;;
esac

popd
